import pandas as pd
import requests
import base64
import io

import re
from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models
import seaborn as sns
import gensim
import string
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
from gensim import similarities

from math import sqrt

from cleaner import clean_text

def pass_content(app, news_data):
    contents = [doc.get("content") for doc in news_data.find({}, {"content": 1})]
    df = pd.DataFrame({"title": contents})
    app.logger.info(df)
    return df

##################

new_en_stop = get_stop_words('en')

ldamodel_r = None
dictionary = None
final_list = None
recommends = None
num_topics = 10

tokenizer = RegexpTokenizer(r'\w+')
p_stemmer = PorterStemmer()


#Recibe un dataframe
def LDA(df_title):
    # df_title.replace({'U.S.':'USA '} , regex=True , inplace = True)
    # df_title.replace({'Trumps':'Trump'} , regex=True , inplace = True)#{'A': 'new'}, regex=True
    # #Convierte a minusculas cada elemento de cada lista
    # lista_lr =[]
    # for rows in df_title.title:
    #     my_list = [rows.lower()]
    #     lista_lr.append(my_list)
    #
    # # Tokens de cada lista
    #
    # flat_list = [item for sublist in lista_lr for item in sublist]
    # list_token = []
    # for sentence in flat_list:
    #     list_token.append(tokenizer.tokenize(sentence))
    #
    # ##Eliminando las stop words
    # #en_stop = get_stop_words('en')
    # stopped_tokens = []
    # temp = []
    # for i in list_token:
    #     for j in i:
    #         if not j in new_en_stop:
    #             temp.append(j)
    #     stopped_tokens.append(temp)
    #     temp = []
    # #####Eliminando las stop words
    # text = []
    # tempo = []
    #
    # for item in stopped_tokens:
    #     for l in item:
    #         st = p_stemmer.stem(l)
    #         if len(st) > 1:
    #             tempo.append(st)
    #     text.append(tempo)
    #     tempo = []

    df_title["title"] = df_title["title"].apply(clean_text)
    tokens = df_title["title"].apply(tokenizer.tokenize).tolist()
    tokens = [[word for word in words if len(word) > 1] for words in tokens]
    text = tokens

    global ldamodel_r
    global dictionary
    global final_list

    # text = [[w for w in words if len(w) > 1] for words in text]
    ###Dicionarios de las lstas y ocurrencias
    dictionary = corpora.Dictionary(text)
    corpus = [dictionary.doc2bow(text) for text in text]

    ldamodel_r = gensim.models.ldamodel.LdaModel(corpus, num_topics=num_topics, id2word = dictionary, passes=10)
    # return print(ldamodel.print_topics(num_topics=numero_topic, num_words=3))

    final_list = ldamodel_r.show_topics(num_words=7 , num_topics = num_topics)



# modelo_news, dicc  = LDA(df_title)


#Grafica de las topics
def plot_topic(idx, app):
    tupla = final_list[idx]
    xy = [(float(pr), to.replace('"', "")) for (pr, to) in [x.strip().split("*") for x in tupla[1].split("+")]]
    x = [x for x, y in xy]
    y = [y for x, y in xy]

    app.logger.info(x)
    app.logger.info(y)

    d = {'words':y,'Prob':x}
    grp = pd.DataFrame(d)
    grp["Prob"] = grp["Prob"].astype("float")
    barras = sns.barplot(x = grp["words"], y =grp["Prob"], palette = 'Blues_d', capsize = 0.05,saturation = 4,
            errcolor = 'black')
    barras.set_title("Topic " + str(idx))

    plt.figure()
    graph_bytes = io.BytesIO()
    barras.figure.savefig(graph_bytes, format='jpg')
    graph_bytes.seek(0)
    graph_b64 = base64.b64encode(graph_bytes.read())
    plt.clf()
    return  graph_b64.decode("utf-8")

####

def grp_nuevo_texto(texto, graph=True):
    clean = clean_text(texto)
    tokens = tokenizer.tokenize(clean)
    texts_nuevo = tokens
    # raw = texto.lower()
    # tokens = tokenizer.tokenize(raw)
    # stopped_tokens = [i for i in tokens if not i in new_en_stop]
    # texts_nuevo = [p_stemmer.stem(i) for i in stopped_tokens]
    tupla = ldamodel_r[dictionary.doc2bow(texts_nuevo)]
    data = {'Belong':[t for t, p in tupla], 'Proba':[p for t, p in tupla]}
    df_per = pd.DataFrame(data)
    df_per = df_per[df_per["Proba"] > 1/num_topics]
    #graph_ = plt.bar(df_per['Pretenencia'].astype(str), df_per["Proba"])
    if graph:
        graph_ = sns.barplot(x = df_per['Belong'].astype(str) , y = df_per["Proba"], palette = 'BuGn_r', errcolor = 'black')

        plt.figure()
        graph_bytes = io.BytesIO()
        graph_.figure.savefig(graph_bytes, format='jpg')
        graph_bytes.seek(0)
        graph_b64 = base64.b64encode(graph_bytes.read())
        plt.clf()
        return  graph_b64.decode("utf-8"), df_per

    else:
        return  None, df_per


#### Recomendacion


#Creacion del dataframe para hacer la recomendacion

def get_df_from_db(news_data, app):
    all_news = news_data.find({}, {"content": 0, "topics": 0, "url": 0, "title": 0})
    all_news_df = pd.concat([pd.DataFrame([list(x.values())], columns=x.keys()) for x in list(all_news)])

    app.logger.info("ALL_NEWS_DF")
    app.logger.info(all_news_df)

    global recommends
    recommends = all_news_df.set_index("_id").T
    recommends.to_csv("recommends.csv", index=False)

    app.logger.info("RECOMMENDS")
    app.logger.info(recommends)

#######

#### Recomendacion calculos


def sim_distance(df, user1, user2):
    diff = df[user1] - df[user2]
    diff = diff.dropna()
    if len(diff) == 0:
        return 0
    pow_diff = diff**2
    sum_pow = sum(pow_diff)
    sqr_sum = sum_pow**(1/2)
    return 1/(1 + sqr_sum)

def sim_pearson(df, user1, user2):
    return df[user1].corr(df[user2])

def similar_users(df, user, n=5, similarity=sim_distance):
    scores = [(similarity(df, user, other_user), other_user) for other_user in df.columns]
    ord_scores = sorted(scores, key=(lambda x: x[0]))[::-1]
    return ord_scores[1:n+1]


def busca_simi(new_df, vlor, app):
    """vlor = _id"""

    df_ = pd.concat([recommends, new_df], axis=1)

    app.logger.info("RECOMMENDS")
    app.logger.info(df_)


    cuatro = similar_users(df_, vlor , n=5, similarity=sim_distance)
    scores, users = zip(*cuatro)
    sim_df = df_[list(users)]
    # mask = df_[vlor].isna()
    # recommendations = df_[mask]#.transpose()
    _series = pd.Series(data=scores, index=users)
    _series.to_frame()

    return _series.to_frame()
